var io = require('socket.io').listen(8888);
var adapter = require('./config/adapter.js');
var router = require('./config/routes.js');

adapter.init(io);
io.users = {};

// Socket on connection events
io.sockets.on('connection', function(socket) {
    Object.keys(router.routes).forEach(function(route) {
        socket.on(route, function(data) {
            router.routing(route, io, socket, data);
        });
    });

    // Socket on disconnect
    socket.on('disconnect', function() {
        delete io.users[socket.id];
    });
});