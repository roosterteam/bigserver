module.exports = {
    getAll: function(io, socket, data){
        io.sockets.emit('/friend/getAll', {code: '200', data: io.users});
    },
    countAll: function(io, socket, data){
        io.sockets.emit('/friend/countAll', {code: '200', data: Object.keys(io.users).length});  
    },
    getFriend: function(io, socket, data){
        
    },
    get: function(io, socket, data){
    	io.models.friend.find({uid: data.uid}, function(err, friends){
    		if(err) {
				socket.emit('/friend/get',{code: 'Có lỗi khi lấy thông tin bạn bè'});
				return;
			}
			var friendUids = [];
			friends.forEach(function(item){
				friendUids.push(item.friendUid);
			});
			io.models.user.find({uid: friendUids}, function(err, users){
				if(err) {
					socket.emit('/friend/get',{code: 'Có lỗi khi lấy thông tin bạn bè'});
					return;
				}
				socket.emit('/friend/get', {code: '200', data: users});
			});
    	});
    },
    add: function(io, socket, data){
    	var uid = data.uid;
    	var friendUid = data.friendUid;
        io.models.friend.find([{uid: uid, friendUid: friendUid}, {uid: friendUid, friendUid: uid}],
            function(err, users){
                if(users.length){
                    socket.emit('/friend/add',{code: 'Đã là bạn bè không thể kết bạn được'});
                }else{
                    io.models.friend.create([{uid: uid, friendUid: friendUid}, {uid: friendUid, friendUid: uid}],
                        function(err, friends){
                            if(err) {
                                socket.emit('/friend/add',{code: 'Có lỗi xảy ra'});
                                return;
                            }
                            socket.emit('/friend/add',{code: '200'});
                        });
                }
            });
    },
    remove: function(io, socket, data){
    	var uid = data.uid;
    	var friendUid = data.friendUid;
    	io.models.friend.destroy({ or: [{uid: uid, friendUid: friendUid}, {uid: friendUid, friendUid: uid}]},
    		function(err, friends){
    			if(err) {
    				socket.emit('/friend/remove',{code: 'Có lỗi xảy ra'});
    				return;
    			}
    			socket.emit('/friend/remove',{code: '200'});
    		});
    }
}