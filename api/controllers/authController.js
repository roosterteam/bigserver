// Auth Controller
var uuid = require('node-uuid');
module.exports = {
    guest: function(io, socket, data){
        var type = data.type;
        var uid = data.uid;
        if(type==='guest'){
            if(!uid){
                // Create random username
                var newUser = {uid: uuid.v1(), type: 'guest'};
                io.models.user.create(newUser, function(err, user) {
                    if (err) throw err;
                    if (user) {
                      socket.emit('/auth/guest', {code: '200', data: user});
                      io.users[socket.id] = user;
                    }
                });
            }else{
                io.models.user.findOne({uid: uid}, function(err, user){
                    if (err) throw err;
                    if (user) {
                        socket.emit('/auth/guest', {code: '200', data: user});
                        io.users[socket.id] = user;
                    }
                });
            }
        }
    },

    // login: function(io, socket, data) {
    //     io.models.user.findOne(data, function(err, user) {
    //         if (err) throw err;
    //         socket.emit('/auth/login', {
    //             code: 200,
    //             data: user
    //         });
    //     });
    // },

    logout: function(io, socket, data) {
        delete io.users[socket.id];
        socket.emit('/auth/logout',{code: '200'});
    },

    fb: function(io, socket, data) {
        var FB = require('fb');
        var accessToken = data.accessToken;
        FB.setAccessToken(accessToken);
        FB.api('fql', {
            q: 'SELECT uid, first_name, last_name, name, email  FROM user WHERE uid=me()'
        }, function(res) {
            if (!res || res.error) {
                socket.emit('/auth/fb', {
                    error: res.error
                });
                return;
            }
            var fbUser = res.data[0];
            fbUser.type = 'fb';
            io.models.user.findOne({
                uid: fbUser.uid
            }, function(err, user) {
                if (err) throw err;
                if (!user) {
                    io.models.user.create(fbUser, function(err, user) {
                        if (err) throw err;
                        if (user) {
                            socket.emit('/auth/fb', {
                                code: '200',
                                data: user
                            });
                            io.users[socket.id] = user;
                        }
                    });
                }else{
                    socket.emit('/auth/fb', {
                        code: '200',
                        data: user
                    });
                    io.users[socket.id] = user;
                }
            });
        });
    }
}