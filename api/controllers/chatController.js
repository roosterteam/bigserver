// Chat Controller
module.exports = {
	message: function(io, socket, data){
		// If private
		if(data.isPrivate){
			var id = '';
			socket.broadcast.to(id).emit('/chat/message', { code: '200', data: data});
		}else{
			socket.broadcast.emit('/chat/message', { code: '200', data: data });
		}
	}
}