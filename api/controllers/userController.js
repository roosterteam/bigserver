// UserController
module.exports = {
  register: function(io, socket, data) {
      io.models.user.create(data, function(err, user) {
        if (err) throw err;
        if (user) {
          console.log('User registered');
        }
      });
  }
}