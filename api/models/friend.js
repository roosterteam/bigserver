var Waterline = require('waterline');

var friend = Waterline.Collection.extend({

  identity: 'friend',
  connection: 'myLocalDisk',
  autoPK: false,
  attributes: {
  	uid: 'string',
  	friendUid: 'string'
  }
});

module.exports = friend;