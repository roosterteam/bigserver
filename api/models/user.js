var Waterline = require('waterline');

var user = Waterline.Collection.extend({

  identity: 'user',
  connection: 'myLocalDisk',
  autoPK: false,
  attributes: {
    uid: 'string',
    socketid: 'string',
    username: {
      type: 'string',
      defaultsTo: 'guest'
    },
    password: {
      type: 'string',
      defaultsTo: '123'
    },
    first_name: {
      type: 'string',
      defaultsTo: ' '
    },
    last_name: {
      type: 'string',
      defaultsTo: ' '
    },
    name: {
      type: 'string',
      defaultsTo: 'Guest'
    },
    email: {
      type: 'string',
      defaultsTo: 'john@example.com'
    },
    phone: {
      type: 'string',
      defaultsTo: '09xxxxxxx'
    },
    win: {
      type: 'int',
      defaultsTo: 0
    },
    lose: {
      type: 'int',
      defaultsTo: 0
    },
    exp: {
      type: 'int',
      defaultsTo: 0
    },
    gold: {
      type: 'int',
      defaultsTo: 0
    },
    maxQuestion: {
      type: 'int',
      defaultsTo: 0
    },
    gems: {
      type: 'int',
      defaultsTo: 0
    },
    avatar: 'string',
    type: 'string' // guest || fb
  }
});

module.exports = user;