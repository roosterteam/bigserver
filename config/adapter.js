var Waterline = require('waterline');
var fs = require('fs');
var path = require('path');

// Instantiate a new instance of the ORM
var orm = new Waterline();

// Require any waterline compatible adapters here
var diskAdapter = require('sails-disk'),
  mysqlAdapter = require('sails-mysql');

// Build A Config Object
var config = {

  // Setup Adapters
  // Creates named adapters that have have been required
  adapters: {
    'default': diskAdapter,
    disk: diskAdapter,
    mysql: mysqlAdapter
  },

  // Build Connections Config
  // Setup connections using the named adapter configs
  connections: {
    myLocalDisk: {
      adapter: 'disk'
    },

    myLocalMySql: {
      adapter: 'mysql',
      host: 'localhost',
      port: 3306,
      user: 'username',
      password: 'password',
      database: 'MySQL Database Name'
    }
  }
};

// Load all models
var modelFiles = fs.readdirSync(path.join(process.cwd(),'api/models/'));
modelFiles.forEach(function(item){
  orm.loadCollection(require('../api/models/'+item));
});

module.exports = {
  orm: orm,
  init: function(io) {
    // Start Waterline passing adapters in
    orm.initialize(config, function(err, models) {
      if (err) throw err;
      io.models = models.collections;
    });
  }
}