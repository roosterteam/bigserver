var fs = require('fs');
var path = require('path');

// Routes should be declared here
module.exports.routes = {
  '/auth/login': {},
  '/auth/logout': {},
  '/auth/fb': {},
  '/auth/guest': {},
  '/friend/getAll': {},
  '/friend/countAll': {},
  '/friend/add': {},
  '/friend/remove': {},
  '/friend/get': {},
  '/chat/message': {},
};

// Load all controllers
var allControllers = {};
var controllerFiles = fs.readdirSync(path.join(process.cwd(), 'api/controllers/'));
controllerFiles.forEach(function(item) {
  var key = item.substr(0, item.length - 'Controller.js'.length);
  allControllers[key] = require('../api/controllers/' + item);
});

module.exports.controllers = allControllers;

// Routing
module.exports.routing = function(route, io, socket, data) {
  if(!this.routes[route]) throw '`'+route+'` route does not exist';
  var controller = this.routes[route].controller;
  var action = this.routes[route].action;

  // If controller and action do not exist, select default
  var routeSeparator = route.split('/');
  if(!controller) controller = routeSeparator[1];
  if(!action) action = routeSeparator[2];

  if (!this.controllers[controller]) throw '`' + controller + 'Controller` does not exist';
  if (!this.controllers[controller][action]) throw '`' + controller + 'Controller` does not have `' + action + '` action';

  this.controllers[controller][action].apply(null, [io, socket, data]);
}